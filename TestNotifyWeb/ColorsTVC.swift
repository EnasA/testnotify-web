//
//  ColorsTVC.swift
//  TestNotifyWeb
//
//  Created by enas on 7/7/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
class ColorsTVC: UITableViewController  {
    
    var allColors:[Color] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allColors = [Color(name: "All"),Color(name: "red "),Color(name: "blue"),Color(name: "green yellow"),Color(name: "cyan")]
        Messaging.messaging().subscribe(toTopic: "colors")

    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        return allColors.count
        
        
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil{
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        }
        cell?.textLabel?.text = allColors[indexPath.row].name
        if allColors[indexPath.row].isSelected
        {
            cell?.accessoryType = .checkmark
        }
        else
        {
            cell?.accessoryType = .none
        }
        cell?.selectionStyle = .none
        return cell!
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            allColors[indexPath.row].isSelected = !allColors[indexPath.row].isSelected
            for index in allColors.indices
            {
                allColors[index].isSelected = allColors[indexPath.row].isSelected
                Messaging.messaging().subscribe(toTopic: "colors")
            }
        }
        else
        {
            allColors[indexPath.row].isSelected = !allColors[indexPath.row].isSelected
            if allColors.dropFirst().filter({ $0.isSelected }).count == allColors.dropFirst().count
            {
                allColors[0].isSelected = true
            }
            else
            {
                allColors[0].isSelected = false
            }
        }
        tableView.reloadData()
    }
    
    
    
    
}

struct Color
{
    var name:String
    var isSelected:Bool! = false
    init(name:String) {
        self.name = name
    }
}

