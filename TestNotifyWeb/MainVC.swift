//
//  MainVC.swift
//  TestNotifyWeb
//
//  Created by enas on 7/3/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ShowHome),
                                               name: NSNotification.Name("ShowHome"),
                                               object: nil)
        /* NotificationCenter.default.addObserver(self,
                                               selector: #selector(showGoogle),
                                               name: NSNotification.Name("showGoogle"),
                                               object: nil)
 */
 
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showColor),
                                               name: NSNotification.Name("showColor"),
                                               object: nil)
        
        
        
    }
    
    @objc func ShowHome() {
        performSegue(withIdentifier: "ShowHome", sender: nil)
    }
    
   /* @objc func showGoogle() {
        performSegue(withIdentifier: "showGoogle", sender: nil)
    }
    */
    @objc func showColor() {
        performSegue(withIdentifier: "showColor", sender: nil)
    }
    
    
    @IBAction func onMoreTapped() {
        print("TOGGLE SIDE MENU")
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
}
