//
//  AppDelegate.swift
//  TestNotifyWeb
//
//  Created by enas on 7/3/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // init firebase
        FirebaseApp.configure()
        
        registerForPushNotifications(application)
        
        return true
    }
    
    private func registerForPushNotifications(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            // for iOS 10 and above
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound], completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
    
    func ConnectToFCM() {
        Messaging.messaging().shouldEstablishDirectChannel = true
        
        if let token = InstanceID.instanceID().token() {
            print("DCS: " + token)
        }
        
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        ConnectToFCM()
    }
    

    
    func applicationWillResignActive(_ application: UIApplication) {
    }

    
   
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        Messaging.messaging().disconnect()
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        ConnectToFCM()
    }

    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    // MARK: - Push notification registeration handler iOS 9 & iOS 10 (unchanged)
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // by default subsribe to
        // notes: apps can subscribe to any topis only after this method is called
   ////  my Topics  here Colors
        
        Messaging.messaging().subscribe(toTopic: "colors")
        Messaging.messaging().subscribe(toTopic: "Red")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Swift.Error) {
        print("Failed to register:", error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("didReceiveRemoteNotification \(userInfo)")
    }
}

// MARK: - Push notification register iOS 10.*
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(.badge)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // handle user info data
        processNotification(response.notification)
        completionHandler()
    }
    
    @available(iOS 10.0, *)
    private func processNotification(_ notif: UNNotification) {
        print(notif.request.content.userInfo)
    }
}
