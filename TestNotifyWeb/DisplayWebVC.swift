//
//  DisplayWebVC.swift
//  TestNotifyWeb
//
//  Created by enas on 7/3/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import UIKit

class DisplayWebVC: UIViewController , UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
override func viewDidLoad(){
   super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    webView.delegate = self
    loadWebPage()
}

    override func didReceiveMemoryWarning(){
       
        super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
}


func loadWebPage(){
    
    let url = URL(string: "https://www.google.com")
    let request = URLRequest(url: url!)
    webView.loadRequest(request)
}

    func webViewDidStartLoad(_ webView: UIWebView){
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
}

    func webViewDidFinishLoad(_ webView: UIWebView){
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
}

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  print("There was a problem loading the web page!")
}

}
   

